import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/store'
import admin from './routes/admin'
import user from './routes/user'
import utils from '@/libs/utils'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior() {
        return { x: 0, y: 0 }
    },
    routes: [{
            path: '/login',
            name: 'login',
            component: () =>
                import ('@/views/Login.vue'),
            meta: {
                layout: 'full',
            },
        },
        {
            path: '/register',
            name: 'register',
            component: () =>
                import ('@/views/Register.vue'),
            meta: {
                layout: 'full',
            },
        },
        {
            path: '/error-404',
            name: 'error-404',
            component: () =>
                import ('@/views/error/Error404.vue'),
            meta: {
                layout: 'full',
            },
        },
        ...admin,
        ...user,
        {
            path: '*',
            redirect: 'error-404',
        },
    ],
})

router.beforeEach(async(to, from, next) => {

        var token = sessionStorage.getItem('token');
        if (token != null) {
            //console.log(token);
            try {
                var resultado = await utils.verifyToken(token, process.env.VUE_APP_SECRET_KEY)
                    //console.log("sesion ",resultado.data);
                var type = null;
                if (resultado.data.id_rol == 0) {
                    type = "user";
                } else {
                    type = "admin";
                }

                const objeto = {
                    token,
                    type
                }
                store.commit('loginIn', objeto)

            } catch (error) {

            }
        }


        if (to.matched.some(route => route.meta.requiresAuth)) {

            if (store.state.isLoggued) {

                if (to.meta.requiresAuth == store.state.type) {

                    next()
                } else if (to.meta.requiresAuth == "admin") {
                    next('/admin/login')
                } else {
                    //console.log("aqui2");
                    //store.commit('logout')
                    next('/login')
                }

            } else {
                //console.log("aqui3 ",to.meta.requiresAuth);
                if (to.meta.requiresAuth == "admin") {
                    //store.commit('logout')
                    next('/admin/login')
                } else {
                    //store.commit('logout')
                    next('/login')
                }

            }
        } else {
            //console.log("aqui4");
            next()
        }

    })
    // ? For splash screen
    // Remove afterEach hook if you are not using splash screen
router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = 'none'
    }
})

export default router