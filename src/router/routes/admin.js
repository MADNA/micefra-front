export default [{
        path: '/admin/login',
        name: 'loginAdmin',
        component: () =>
            import ('@/views/admin/LoginAdmin.vue'),
        meta: {
            pageTitle: 'Login',
            layout: 'full',
        },
    },
    {
        path: '/admin',
        name: 'Admin',
        component: () =>
            import ('@/views/admin/Home.vue'),
        meta: {
            pageTitle: 'Inicio',
            icon: "GridIcon",
            breadcrumb: [{
                text: 'Inicio',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/customers',
        name: 'customers',
        component: () =>
            import ('@/views/admin/CustomersPage.vue'),
        meta: {
            pageTitle: 'Clientes',
            icon: 'UsersIcon',
            breadcrumb: [{
                text: 'Clientes',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/messages',
        name: 'admin-messages',
        component: () =>
            import ('@/views/admin/MessagesPage.vue'),
        meta: {
            pageTitle: 'Mensajeria',
            icon: 'MailIcon',
            breadcrumb: [{
                text: 'Mensajeria',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/purses',
        name: 'purses',
        component: () =>
            import ('@/views/admin/PursersPage.vue'),
        meta: {
            pageTitle: 'Monederos',
            icon: 'CreditCardIcon',
            breadcrumb: [{
                text: 'Monederos',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/packages',
        name: 'admin-packages',
        component: () =>
            import ('@/views/admin/PackagePage.vue'),
        meta: {
            pageTitle: 'Paquetes',
            icon: 'BoxIcon',
            breadcrumb: [{
                text: 'Paquetes pages',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/users',
        name: 'users',
        component: () =>
            import ('@/views/admin/UsersPage.vue'),
        meta: {
            pageTitle: 'Usuarios',
            icon: 'UserIcon',
            breadcrumb: [{
                text: 'Usuarios pages',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/roles',
        name: 'roles',
        component: () =>
            import ('@/views/admin/RolesPage.vue'),
        meta: {
            pageTitle: 'Roles',
            icon: 'SettingsIcon',
            breadcrumb: [{
                text: 'Roles pages',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/account-setting',
        name: 'admin-account-setting',
        component: () =>
            import ('@/views/admin/account-setting/AccountSetting.vue'),
        meta: {
            pageTitle: 'Perfil',
            icon: 'GridIcon',
            breadcrumb: [{
                text: 'Perfil',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/withdrawal',
        name: 'admin-withdrawal',
        component: () =>
            import ('@/views/admin/WithdrawalsPage.vue'),
        meta: {
            pageTitle: 'Retiros clientes',
            icon: 'UserMinusIcon',
            breadcrumb: [{
                text: 'Retiros',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    },
    {
        path: '/admin/deposits',
        name: 'admin-deposits',
        component: () =>
            import ('@/views/admin/DepositsPage.vue'),
        meta: {
            pageTitle: 'Depósitos clientes',
            icon: 'UserPlusIcon',
            breadcrumb: [{
                text: 'Depósitos',
                active: true,
            }, ],
            requiresAuth: 'admin'
        },
    }
]