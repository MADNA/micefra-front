export default [{
        path: '/',
        name: 'home',
        component: () =>
            import ('@/views/Home.vue'),
        meta: {
            pageTitle: 'Inicio',
            icon: 'GridIcon',
            breadcrumb: [{
                text: 'Inicio',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/second-page',
        name: 'second-page',
        component: () =>
            import ('@/views/SecondPage.vue'),
        meta: {
            pageTitle: 'Second Page',
            breadcrumb: [{
                text: 'Second Page',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/packages',
        name: 'packages',
        component: () =>
            import ('@/views/user/PackagePage.vue'),
        meta: {
            pageTitle: 'Paquetes',
            icon: 'BoxIcon',
            contentClass: 'ecommerce-application',
            breadcrumb: [{
                text: 'Paquetes',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/my-packages',
        name: 'my-packages',
        component: () =>
            import ('@/views/user/OrdersPage.vue'),
        meta: {
            pageTitle: 'Mis paquetes',
            icon: 'BoxIcon',
            contentClass: 'ecommerce-application',
            breadcrumb: [{
                text: 'Mis paquetes',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/messages',
        name: 'messages',
        component: () =>
            import ('@/views/user/MessagesPage.vue'),
        meta: {
            pageTitle: 'Mensajeria',
            icon: 'MailIcon',
            breadcrumb: [{
                text: 'Mensajeria',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/withdrawals',
        name: 'withdrawals',
        component: () =>
            import ('@/views/user/WithdrawalsPage.vue'),
        meta: {
            pageTitle: 'Retiros',
            icon: 'MinusCircleIcon',
            breadcrumb: [{
                text: 'Retiros',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/deposits',
        name: 'deposits',
        component: () =>
            import ('@/views/user/DepositsPage.vue'),
        meta: {
            pageTitle: 'Depósitos',
            icon: 'PlusCircleIcon',
            breadcrumb: [{
                text: 'Depósitos',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/direct-affiliates',
        name: 'direct-affiliates',
        component: () =>
            import ('@/views/user/DirectAfiliatePage.vue'),
        meta: {
            pageTitle: 'Afiliados directos',
            icon: 'ZapIcon',
            breadcrumb: [{
                text: 'Afiliados directos Page',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/indirect-affiliates',
        name: 'indirect-affiliates',
        component: () =>
            import ('@/views/user/IndirectAfiliatePage.vue'),
        meta: {
            pageTitle: 'Afiliados indirectos',
            icon: 'ZapOffIcon',
            breadcrumb: [{
                text: 'Afiliados indirectos Page',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    },
    {
        path: '/account-setting',
        name: 'account-setting',
        component: () =>
            import ('@/views/user/account-setting/AccountSetting.vue'),
        meta: {
            pageTitle: 'Perfil',
            icon: 'UserIcon',
            breadcrumb: [{
                text: 'Perfil',
                active: true,
            }, ],
            requiresAuth: 'user'
        },
    }
]