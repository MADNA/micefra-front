const jwt = require('jsonwebtoken')

export default {
	verifyToken(token, secret, options) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, secret, (err, decoded) => {
            if (err) return reject(err)

            resolve(decoded)
            })
        })
    }
}