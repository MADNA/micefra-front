import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
	baseURL: process.env.VUE_APP_URL_BASE,
	withCredentials: false,
	headers : {
		Accept: 'application/json',
		'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
	}
});

export default {
    ListarMonederos(){
		return apiClient.get('admin/monederos/listar.php');
	},
	Nuevo(post){
		return apiClient.post('admin/monederos/guardar.php', post);
	},
	Editar(post){
		return apiClient.post('admin/monederos/editar.php', post);
	},
	Eliminar(post){
		return apiClient.post('admin/monederos/eliminar.php', post);
	},
}