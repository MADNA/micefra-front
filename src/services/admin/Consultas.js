import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {

    // ListarPaquetes(){
    // 	return apiClient.get('admin/paquetes/listar.php');
    // },
    ListarRoles() {
        return apiClient.get('admin/roles/roles.php');
    },
    ListarMonederos() {
        return apiClient.get('admin/monederos/listar.php');
    },

    ListarUsuarios() {
        return apiClient.get('admin/usuarios/listar.php');
    },
    ListarClientes() {
        return apiClient.get('admin/clientes/listar.php');
    },
    ListarRetiros() {
        return apiClient.get('admin/transacciones/retiros.php');
    },
    ListarDepositos() {
        return apiClient.get('admin/transacciones/depositos.php');
    },
    ListarClienteReferidos(post) {
        return apiClient.post('admin/clientes/cliente_afiliados.php', post)
    },
    ConsultarMontoTotal(post) {
        return apiClient.post('admin/transacciones/monto_total.php', post)
    }
}