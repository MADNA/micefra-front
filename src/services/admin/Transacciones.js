import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {

    VerificarDeposito(post) {
        return apiClient.post('admin/transacciones/verificar_deposito.php', post)
    },
    RechazarDeposito(post) {
        return apiClient.post('admin/transacciones/rechazar_deposito.php', post)
    },
    VerificarRetiro(post) {
        return apiClient.post('admin/transacciones/verificar_retiro.php', post)
    },
    RechazarRetiro(post) {
        return apiClient.post('admin/transacciones/rechazar_retiro.php', post)
    }
}