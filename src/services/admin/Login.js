import axios from 'axios'

const apiClient = axios.create({
	baseURL: process.env.VUE_APP_URL_BASE,
	withCredentials: false,
	headers : {
		Accept: 'application/json',
		'Content-Type': 'application/json'
	}
});

export default {
	Login(post){
		return apiClient.post('admin/sesion/login.php',post);
	}
}