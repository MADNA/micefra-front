import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {

    Guardar(post) {
        return apiClient.post('admin/clientes/registrar.php', post)
    },

    Editar(post) {
        return apiClient.post('admin/clientes/editar.php', post)
    },

    Verificar(post) {
        return apiClient.post('admin/clientes/verificar.php', post)
    },

    Eliminar(post) {
        return apiClient.post('admin/clientes/eliminar.php', post)
    }
}