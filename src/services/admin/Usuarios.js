import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {

    CrearUsuario(post) {
        return apiClient.post('admin/usuarios/guardar.php', post)
    },
    EditarUsuario(post) {
        return apiClient.post('admin/usuarios/editar.php', post)
    },
    EliminarUsuario(post) {
        return apiClient.post('admin/usuarios/eliminar.php', post)
    }
}