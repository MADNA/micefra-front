import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {
    ConsultarClientesMensajes() {
        return apiClient.get('admin/mensajeria/consultar_clientes_mensajes.php')
    },
    EnviarMensaje(post) {
        return apiClient.post('admin/mensajeria/enviar_mensaje.php', post)
    }
}