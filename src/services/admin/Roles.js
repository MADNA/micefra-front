import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
	baseURL: process.env.VUE_APP_URL_BASE,
	withCredentials: false,
	headers : {
		Accept: 'application/json',
		'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
	}
});

export default {
	ListarRoles(){
		return apiClient.get('admin/roles/roles.php');
	},
    ListarPermisologia(){
		return apiClient.get('admin/roles/permisologia.php');
	},
    Nuevo(post){
		return apiClient.post('admin/roles/guardar.php', post);
	},
    Editar(post){
		return apiClient.post('admin/roles/editar.php', post);
	},
	Eliminar(post){
		return apiClient.post('admin/roles/eliminar.php', post);
	},
    
}