import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {
    ListarPaquetes() {
        return apiClient.get('admin/paquetes/listar.php');
    },
    Nuevo(post) {
        return apiClient.post('admin/paquetes/guardar.php', post);
    },
    Editar(post) {
        return apiClient.post('admin/paquetes/editar.php', post);
    },
    Eliminar(post) {
        return apiClient.post('admin/paquetes/eliminar.php', post);
    },
    EditarImagen(post) {
        return apiClient.post('admin/paquetes/editar_imagen.php', post)
    }
}