import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {
    EditarDatos(post) {
        return apiClient.post('sesion/updateProfile.php', post);
    },
    EditarPassword(post) {
        return apiClient.post('sesion/updatePassword.php', post);
    },
    ConsultarSaldo(post) {
        return apiClient.post('user/consultar_saldo.php', post)
    }
}