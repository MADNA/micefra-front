import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {
    ConsultarMensajes() {
        return apiClient.get('user/mensajeria/consultar_mensajes.php')
    },
    EnviarMensaje(post) {
        return apiClient.post('user/mensajeria/enviar_mensaje.php', post)
    }
}