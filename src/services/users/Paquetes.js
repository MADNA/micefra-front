import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {
    Paquetes() {
        return apiClient.post('user/paquetes/listar.php');
    },
    Comprar(post) {
        return apiClient.post('user/paquetes/comprar_paquete.php', post)
    },
    ConsultarOrdenes(post) {
        return apiClient.post('user/paquetes/listar_ordenes.php', post)
    },
    Retirar(post) {
        return apiClient.post('user/paquetes/retirar_paquete.php', post)
    }
}