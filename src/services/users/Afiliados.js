import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
	baseURL: process.env.VUE_APP_URL_BASE,
	withCredentials: false,
	headers : {
		Accept: 'application/json',
		'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
	}
});

export default {
	Directos(post){
		return apiClient.post('user/afiliados/directos.php',post);
	},
	Indirectos(post){
		return apiClient.post('user/afiliados/indirectos.php',post);
	}
}