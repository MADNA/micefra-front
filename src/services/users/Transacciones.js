import axios from 'axios'
import store from '@/store/store'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + store.state.token
    }
});

export default {
    Depositos(post) {
        return apiClient.post('user/transacciones/depositos.php', post);
    },
    Retiros(post) {
        return apiClient.post('user/transacciones/retiros.php', post);
    },
    ListaMonederos() {
        return apiClient.post('user/transacciones/monederos.php');
    },
    NuevoD(post) {
        return apiClient.post('user/transacciones/depositar.php', post);
    },
    Retirar(post) {
        return apiClient.post('user/transacciones/retirar.php', post);
    },
    ConsultarSaldo(post) {
        return apiClient.post('user/consultar_saldo.php', post)
    }
}