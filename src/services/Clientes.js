import axios from 'axios'

const apiClient = axios.create({
    baseURL: process.env.VUE_APP_URL_BASE,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
});

export default {
    Guardar(post) {
        return apiClient.post('registro/registro.php', post);
    }

}