export default [{
        title: 'Inicio',
        route: 'home',
        icon: 'GridIcon',
    },
    {
        title: 'Paquetes',
        route: 'packages',
        icon: 'BoxIcon',
    },
    {
        title: 'Mis Paquetes',
        route: 'my-packages',
        icon: 'BoxIcon',
    },
    {
        title: 'Mensajeria',
        route: 'messages',
        icon: 'MailIcon',
    },
    {
        title: 'Retiros',
        route: 'withdrawals',
        icon: 'MinusCircleIcon',
    },
    {
        title: 'Afiliados directos',
        route: 'direct-affiliates',
        icon: 'ZapIcon',
    },
    {
        title: 'Afiliados indirectos',
        route: 'indirect-affiliates',
        icon: 'ZapOffIcon',
    },
    {
        title: 'Depositos',
        route: 'deposits',
        icon: 'PlusCircleIcon',
    },
]