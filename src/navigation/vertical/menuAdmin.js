export default [{
        title: 'Inicio',
        route: 'Admin',
        icon: 'GridIcon',
    },
    {
        title: 'Clientes',
        route: 'customers',
        icon: 'UsersIcon',
    },
    {
        title: 'Mensajeria',
        route: 'admin-messages',
        icon: 'MailIcon',
    },
    {
        title: 'Monederos',
        route: 'purses',
        icon: 'CreditCardIcon',
    },
    {
        title: 'Paquetes',
        route: 'admin-packages',
        icon: 'BoxIcon',
    },
    {
        title: 'Usuarios',
        route: 'users',
        icon: 'UserIcon',
    },
    {
        title: 'Roles',
        route: 'roles',
        icon: 'SettingsIcon',
    },
    {
        title: 'Retiros clientes',
        route: 'admin-withdrawal',
        icon: 'UserMinusIcon',
    },
    {
        title: 'Depósitos clientes',
        route: 'admin-deposits',
        icon: 'UserPlusIcon',
    },
]