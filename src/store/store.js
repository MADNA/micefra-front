import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({

    state: {
        isLoggued: false,
        token: '',
        type: null,
        saldo: 0
    },
    mutations: {
        loginIn(state, data) {
            state.isLoggued = true;
            state.token = data.token;
            state.type = data.type;
        },
        logout(state) {
            state.isLoggued = false;
            state.token = '';
            state.type = null;
            sessionStorage.removeItem('token');
        },

    },
    actions: {

    },
    modules: {}

});